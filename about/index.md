---
title: "Krusader - Twin panel file management for your desktop"
layout: page
---

# About this website

This website was developed by Dirk Eschler and put into the hands of the [Krusader Krew](../get-involved/contributors/index.html). Rights are reserved unless otherwise stated.

* Some icons used on this website are taken from [David Vignoni](http://www.icon-king.com/)'s excellent Nuvola icon set. They are published under the [GNU Lesser General Public License (LGPL)](http://www.gnu.org/copyleft/lesser.html).
