---
title: "Krusader - Twin panel file management for your desktop"
layout: page
---

# Twin panel file management for your desktop

Krusader is an advanced twin panel (commander style) file manager for
[KDE Plasma](https://kde.org/plasma-desktop) and other desktops in the
\*nix world, similar to [Midnight](https://midnight-commander.org/) or
[Total Commander](https://www.ghisler.com/).
It provides all the file management features you could possibly want. Plus:

- Extensive archive handling
- Mounted filesystem support
- File transfers with (S)FTP
- Advanced search module
- An internal viewer and editor
- Directory synchronisation
- File content comparisons
- Powerful batch renaming
- And much much more...

It supports a wide variety of archive formats and can handle other KIO slaves such
as smb or fish. It is (almost) completely customizable, very user friendly, fast
and looks great on your desktop! You should give it a try.

This piece of software is developed by the [Krusader Krew](/get-involved/contributors/index.html),
published under the [GNU General Public Licence](https://www.gnu.org/copyleft/gpl.html).
